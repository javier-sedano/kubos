const newrelic = require('newrelic');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const history = require('connect-history-api-fallback');
const schedule = require('node-schedule');
const constants = require('./src/constants');
const winstonConfigurer = require('./src/config/winston.configurer');
const kubosService = require('./src/services/kubos.service');

const app = express();

configureReqIdMiddleware(app);
configureMorgan(app);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

winstonConfigurer.configure();
configureRoutes(app);
configureStatic(app);
configureSpa(app);
configureDeleteOldKubosesSched();
configureCustomMetrics();

if (winstonConfigurer === undefined) {
    console.log("No llega");
}

module.exports = app;

function configureReqIdMiddleware(app) {
    const uuid = require('uuid');
    const httpContext = require('express-http-context');
    app.use(httpContext.middleware);
    app.use(function (req, res, next) {
        httpContext.set('reqId', uuid.v4());
        next();
    });
}

function configureMorgan(app) {
    const morgan = require('morgan');
    const httpContext = require('express-http-context');
    morgan.token('reqId', (req, res) => { return httpContext.get('reqId'); })
    const formatterMorgan = morgan(':reqId :remote-addr - :remote-user [:date[iso]] ":method :url HTTP/:http-version" :status :res[content-length] B :response-time ms');
    app.use(formatterMorgan);
}

function configureRoutes(app) {
    const rootRouter = require('./src/routes/root.route');
    app.use(constants.rootUrl, rootRouter);
    const versionRouter = require('./src/routes/version.route');
    app.use(constants.baseRest + '/version', versionRouter);
    const kubosRouter = require('./src/routes/kubos.route');
    app.use(constants.baseRest + '/kubos', kubosRouter);
}

function configureStatic(app) {
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(constants.baseWeb, express.static(path.join(__dirname, 'front/dist')));
}

function configureSpa(app) {
    const winston = require('winston');
    app.use(history({
        index: constants.baseWeb + '/index.html',
        logger: (message, ...optionalParams) => {
            winston.debug(message, optionalParams)
        },
    }));
    // https://github.com/bripkens/connect-history-api-fallback/blob/master/examples/static-files-and-index-rewrite/README.md#configuring-the-middleware
    configureStatic(app);
}

function configureDeleteOldKubosesSched() {
    const NEVER = '0 0 0 31 2 *';
    const DELETE_OLD_CRON = process.env.KUBOS_DELETE_OLD_CRON || NEVER;
    schedule.scheduleJob(DELETE_OLD_CRON, function () {
        console.log("Cron run deleteOldKuboses");
        kubosService.deleteOldKuboses();
    });
}

function configureCustomMetrics() {
    const EVERY_MINUTE = '0 * * * * *';
    schedule.scheduleJob(EVERY_MINUTE, async function () {
        await kubosAmmountCustomMetric();
    });
}

async function kubosAmmountCustomMetric() {
    const KUBOS_AMMOUNT_CUSTOM_METRIC = "Kubos/Ammount";
    const kubosAmmount = await kubosService.count();
    newrelic.recordMetric(KUBOS_AMMOUNT_CUSTOM_METRIC, kubosAmmount);
}