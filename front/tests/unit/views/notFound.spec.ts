import { shallowMount } from '@vue/test-utils';
import NotFound from '../../../src/views/NotFound.vue';

describe('NotFound', () => {
  it('should create', () => {
    const wrapper = shallowMount(NotFound);
    expect(wrapper).toBeDefined();
    expect(wrapper.element).toMatchSnapshot("initial");
  });
});
