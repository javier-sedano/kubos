import { shallowMount } from '@vue/test-utils';
import { setActivePinia, createPinia } from 'pinia'
import axios, { AxiosError, AxiosHeaders } from 'axios';
import KubosView from '../../../src/views/KubosView.vue';
import { useErrorStore } from "../../../src/stores/error.store";
import testConstants from "../tests.constants";
import { Kubos } from '../../../src/services/kubos.service';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

jest.mock('vue-router', () => ({
  useRoute: jest.fn(),
}))

const kubosUrl = testConstants.restBaseUrl + "/kubos/";

const consoleErrorSpy = jest.spyOn(console, "error");

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('Kubos', () => {
  it('should create with empty key', async () => {
    const errorStore = useErrorStore();
    const route = {
      params: { id: "idid" },
      query: { key: null },
    };
    require('vue-router').useRoute.mockReturnValueOnce(route);
    const aTitle = "A good title";
    const aBasicKubos: Kubos = { id: route.params.id, title: aTitle };
    mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: aBasicKubos }));
    const wrapper = shallowMount(KubosView, {
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.kubos).toBeNull();
    await wrapper.vm.$nextTick();
    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(kubosUrl + route.params.id, { params: { key: null } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.kubos).toEqual(aBasicKubos);
    expect(wrapper.element).toMatchSnapshot("user initial");
    expect(errorStore.errorMessage).toBeNull();
  });

  it('should create without key', async () => {
    const errorStore = useErrorStore();
    const route = {
      params: { id: "idid" },
      query: {},
    };
    require('vue-router').useRoute.mockReturnValueOnce(route);
    const aTitle = "A good title";
    const aBasicKubos: Kubos = { id: route.params.id, title: aTitle };
    mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: aBasicKubos }));
    const wrapper = shallowMount(KubosView, {
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.kubos).toBeNull();
    await wrapper.vm.$nextTick();
    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(kubosUrl + route.params.id, { params: { key: null } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.kubos).toEqual(aBasicKubos);
    expect(wrapper.element).toMatchSnapshot("user initial");
    expect(errorStore.errorMessage).toBeNull();
  });

  it('should create detecting error', async () => {
    const errorStore = useErrorStore();
    const route = {
      params: { id: "idid" },
      query: { key: null },
    };
    require('vue-router').useRoute.mockReturnValueOnce(route);
    const anError = "An error";
    mockedAxios.get.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    consoleErrorSpy.mockImplementationOnce(() => { });
    const wrapper = shallowMount(KubosView, {
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.kubos).toBeNull();
    await wrapper.vm.$nextTick();
    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(kubosUrl + route.params.id, { params: { key: null } });
    await wrapper.vm.$nextTick();
    expect(errorStore.errorMessage).toEqual(`Error: ${anError}`);
    errorStore.clearErrorMessage();
    expect(consoleErrorSpy).toHaveBeenCalled();
    expect(wrapper.vm.kubos).toBeNull();
  });

  it('should create detecting Axios 404 error', async () => {
    const errorStore = useErrorStore();
    const route = {
      params: { id: "idid" },
      query: { key: null },
    };
    require('vue-router').useRoute.mockReturnValueOnce(route);
    const anError = "A 404 error"; // TODO: remove?
    const a404Error = new AxiosError()
    a404Error.response = {
      data: {},
      status: 404,
      statusText: "Not found",
      headers: {},
      config: {
        headers: new AxiosHeaders(),
      },
    };
    mockedAxios.get.mockImplementationOnce(() => Promise.reject(a404Error));
    const wrapper = shallowMount(KubosView, {
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.kubos).toBeNull();
    await wrapper.vm.$nextTick();
    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(kubosUrl + route.params.id, { params: { key: null } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.kubos).toBeNull();
    expect(errorStore.errorMessage).toEqual(null);
  });

  it('should create with key', async () => {
    const route = {
      params: { id: "idid" },
      query: { key: "keykey" }
    };
    require('vue-router').useRoute.mockReturnValueOnce(route);
    const aTitle = "A good title";
    const aFullKubos: Kubos = { id: route.params.id, title: aTitle, cards: [{ text: "card1" }, { text: "card2" }] };
    mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: aFullKubos }));
    const wrapper = shallowMount(KubosView, {
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.kubos).toBeNull();
    await wrapper.vm.$nextTick();
    expect(mockedAxios.get).toHaveBeenCalledTimes(1);
    expect(mockedAxios.get).toHaveBeenCalledWith(kubosUrl + route.params.id, { params: { key: route.query.key } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.kubos).toEqual(aFullKubos);
    expect(wrapper.element).toMatchSnapshot("owner initial");
  });

});
