import { shallowMount } from '@vue/test-utils';
import { setActivePinia, createPinia } from 'pinia'
import KubosCreate from '../../../src/views/KubosCreate.vue';
import axios from 'axios';
import { useErrorStore } from "../../../src/stores/error.store";
import testConstants from "../tests.constants";

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const mockRouterPush = jest.fn();
jest.mock('vue-router', () => ({
  useRouter: jest.fn(() => ({
    push: mockRouterPush,
  })),
}));

const kubosUrl = testConstants.restBaseUrl + "/kubos/";

const consoleErrorSpy = jest.spyOn(console, "error");

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('KubosCreate', () => {

  it('should create empty', async () => {
    const wrapper = shallowMount(KubosCreate, {
      global: {
        stubs: ['font-awesome-icon']
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.title).toEqual("");
    expect(wrapper.vm.isCreateDisabled).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("initial");
  });

  it('should create a kubos', async () => {
    const errorStore = useErrorStore();
    const wrapper = shallowMount(KubosCreate, {
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    const aTitle = "A title";
    const anId = "idid";
    const aKey = "keykey";
    wrapper.vm.title = aTitle;
    expect(wrapper.vm.isCreateDisabled).toEqual(false);
    mockedAxios.post.mockImplementationOnce(() => Promise.resolve({ data: { id: anId, key: aKey } }));
    wrapper.vm.create();
    expect(wrapper.vm.isCreateDisabled).toEqual(true);
    await wrapper.vm.$nextTick();
    expect(mockedAxios.post).toHaveBeenCalledTimes(1);
    expect(mockedAxios.post).toHaveBeenCalledWith(kubosUrl, { title: aTitle });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isCreateDisabled).toEqual(false);
    expect(mockRouterPush).toHaveBeenCalledWith({
      name: "KubosView",
      params: { id: anId },
      query: { key: aKey },
    });
    expect(errorStore.errorMessage).toBeNull();
  });

  it('should detect an error creating a kubos', async () => {
    const errorStore = useErrorStore();
    const wrapper = shallowMount(KubosCreate, {
      global: {
        stubs: ['font-awesome-icon']
      },
    });
    const aTitle = "A title";
    const anError = "An error";
    wrapper.vm.title = aTitle;
    mockedAxios.post.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    consoleErrorSpy.mockImplementationOnce(() => { });
    wrapper.vm.create();
    expect(wrapper.vm.isCreateDisabled).toEqual(true);
    await wrapper.vm.$nextTick();
    expect(mockedAxios.post).toHaveBeenCalledTimes(1);
    expect(mockedAxios.post).toHaveBeenCalledWith(kubosUrl, { title: aTitle });
    await wrapper.vm.$nextTick();
    expect(errorStore.errorMessage).toEqual(`Error: ${anError}`);
    errorStore.clearErrorMessage();
    expect(consoleErrorSpy).toHaveBeenCalled();
    expect(wrapper.vm.isCreateDisabled).toEqual(false);
    expect(mockRouterPush).not.toHaveBeenCalled();
  });

});
