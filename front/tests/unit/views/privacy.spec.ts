import { shallowMount } from '@vue/test-utils';
import Privacy from '../../../src/views/Privacy.vue';

describe('Privacy', () => {
  it('should create', () => {
    const wrapper = shallowMount(Privacy);
    expect(wrapper).toBeDefined();
    expect(wrapper.element).toMatchSnapshot("initial");
  });
});
