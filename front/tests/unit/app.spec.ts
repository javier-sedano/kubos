import { shallowMount } from '@vue/test-utils';
import App from '../../src/App.vue';
import axios from 'axios';
import testConstants from "./tests.constants";
import { jest, describe, expect } from '@jest/globals';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const versionUrl = testConstants.restBaseUrl + "/version";

afterEach(() => {
  jest.clearAllMocks();
});

describe('App', () => {
  async function testCreatedWithLoadedVersion() {
    const aVersion = "1.2.3";
    mockedAxios.get.mockImplementationOnce(() => Promise.resolve({ data: aVersion }));
    const wrapper = shallowMount(App, {
      global: {
        stubs: ['router-link', 'router-view', 'font-awesome-icon', 'error-render'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.version).toEqual("");
    await wrapper.vm.$nextTick();
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(versionUrl);
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.version).toEqual(aVersion);
    return wrapper;
  }

  it('should create with version', async () => {
    const wrapper = await testCreatedWithLoadedVersion();
    expect(wrapper.element).toMatchSnapshot("initial");
  });

});


