import { shallowMount } from '@vue/test-utils';
import { setActivePinia, createPinia } from 'pinia'
import ErrorRender from '../../../src/components/ErrorRender.vue';
import { useErrorStore } from "../../../src/stores/error.store";

beforeEach(() => {
  setActivePinia(createPinia());
});

describe('Error.vue', () => {
  it('should create', () => {
    const wrapper = shallowMount(ErrorRender, {
      global: {
        stubs: ['font-awesome-icon']
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.element).toMatchSnapshot("initial");
  });

  it('should show error when stored and dismiss', () => {
    const errorStore = useErrorStore();
    const anError = "An error";
    const wrapper = shallowMount(ErrorRender, {
      global: {
        stubs: ['font-awesome-icon']
      },
    });
    expect(wrapper.vm.errorMessage).toEqual(null);
    expect(wrapper.element).toMatchSnapshot("initial");
    errorStore.setErrorMessage(anError);
    expect(wrapper.vm.errorMessage).toEqual(anError);
    expect(wrapper.element).toMatchSnapshot("showing");
    wrapper.vm.dismissError();
    expect(wrapper.vm.errorMessage).toEqual(null);
    expect(wrapper.element).toMatchSnapshot("initial again");
  });
});
