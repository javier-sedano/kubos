import { shallowMount } from '@vue/test-utils';
import { setActivePinia, createPinia } from 'pinia'
import KubosAddCard from '../../../src/components/KubosAddCard.vue';
import axios from 'axios';
import { useErrorStore } from "../../../src/stores/error.store";
import testConstants from "../tests.constants";

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const kubosUrl = testConstants.restBaseUrl + "/kubos/";
const changeSampleDelay = 5000;

const consoleErrorSpy = jest.spyOn(console, "error");

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('KubosAddCard', () => {
  it('should create and change sample', async () => {
    jest.useFakeTimers();
    const anId = "anId";
    const wrapper = shallowMount(KubosAddCard, {
      props: {
        id: anId,
      }
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.cardText).toEqual("");
    expect(wrapper.vm.isAddDisabled).toEqual(true);
    expect(wrapper.vm.isAddedVisible).toEqual(false);
    expect(wrapper.vm.currentSample).toEqual(0);
    expect(wrapper.vm.sampleCard).not.toMatch(new RegExp("^Lorem ipsum"));
    expect(wrapper.element).toMatchSnapshot("initial");
    jest.advanceTimersByTime(changeSampleDelay + 1);
    expect(wrapper.vm.currentSample).toEqual(1);
  });

  it('should add a card', async () => {
    const errorStore = useErrorStore();
    const anId = "anId";
    const wrapper = shallowMount(KubosAddCard, {
      props: {
        id: anId,
      }
    });
    const aText = "Some thank you text";
    wrapper.vm.cardText = aText;
    expect(wrapper.vm.isAddDisabled).toEqual(false);
    mockedAxios.put.mockImplementationOnce(() => Promise.resolve());
    wrapper.vm.addCard();
    expect(wrapper.vm.isAddDisabled).toEqual(true);
    await wrapper.vm.$nextTick();
    expect(mockedAxios.put).toHaveBeenCalledTimes(1);
    expect(mockedAxios.put).toHaveBeenCalledWith(kubosUrl + anId, { text: aText });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.cardText).toEqual("");
    expect(wrapper.vm.isAddedVisible).toEqual(true);
    expect(wrapper.vm.isAddDisabled).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("after add");
    wrapper.vm.cardText = aText + "2";
    expect(wrapper.vm.isAddedVisible).toEqual(false);
    expect(wrapper.vm.isAddDisabled).toEqual(false);
    expect(errorStore.errorMessage).toBeNull();
  });

  it('should add a card and detect error', async () => {
    const errorStore = useErrorStore();
    const anId = "anId";
    const wrapper = shallowMount(KubosAddCard, {
      props: {
        id: anId,
      }
    });
    const aText = "Some thank you text";
    const anError = "An error";
    wrapper.vm.cardText = aText;
    expect(wrapper.vm.isAddDisabled).toEqual(false);
    mockedAxios.put.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    consoleErrorSpy.mockImplementationOnce(() => { });
    wrapper.vm.addCard();
    expect(wrapper.vm.isAddDisabled).toEqual(true);
    await wrapper.vm.$nextTick();
    expect(mockedAxios.put).toHaveBeenCalledTimes(1);
    expect(mockedAxios.put).toHaveBeenCalledWith(kubosUrl + anId, { text: aText });
    await wrapper.vm.$nextTick();
    expect(errorStore.errorMessage).toEqual(`Error: ${anError}`);
    errorStore.clearErrorMessage();
    expect(consoleErrorSpy).toHaveBeenCalled();
    expect(wrapper.vm.added).toEqual(false);
    expect(wrapper.vm.cardText).toEqual(aText);
    expect(wrapper.vm.isAddedVisible).toEqual(false);
    expect(wrapper.vm.isAddDisabled).toEqual(false);
  });

});
