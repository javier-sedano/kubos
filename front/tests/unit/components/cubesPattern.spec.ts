import { shallowMount } from '@vue/test-utils';
import CubesPattern from '../../../src/components/CubesPattern.vue';

describe('CubesPattern.vue', () => {
  it('should create', async () => {
    const wrapper = shallowMount(CubesPattern);
    expect(wrapper).toBeDefined();
    expect(wrapper.element).toMatchSnapshot("initial");
  });

});
