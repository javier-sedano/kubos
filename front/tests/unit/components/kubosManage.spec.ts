import { shallowMount } from '@vue/test-utils';
import { setActivePinia, createPinia } from 'pinia'
import KubosManage from '../../../src/components/KubosManage.vue';
import axios from 'axios';
import { useErrorStore } from "../../../src/stores/error.store";
import testConstants from "../tests.constants";

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const mockRouterPush = jest.fn();
jest.mock('vue-router', () => ({
  useRouter: jest.fn(() => ({
    push: mockRouterPush,
  })),
}));

const kubosUrl = testConstants.restBaseUrl + "/kubos/";

const consoleErrorSpy = jest.spyOn(console, "error");

Object.defineProperty(window, 'location', {
  writable: true,
  value: { assign: jest.fn() },
});

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('KubosManage', () => {
  it('should create', async () => {
    const anId = "anId";
    const aKey = "aKey";
    const url = "https://www/kubos/" + anId;
    const ownerUrl = url + "?key=" + aKey;
    window.location.href = ownerUrl;
    const wrapper = shallowMount(KubosManage, {
      props: {
        id: anId,
        kkey: aKey,
      },
      global: {
        stubs: ['font-awesome-icon']
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.isDeleteDisabled).toEqual(true);
    expect(wrapper.vm.url).toEqual(url);
    expect(wrapper.vm.ownerUrl).toEqual(ownerUrl);
    expect(wrapper.element).toMatchSnapshot("initial");
  });

  it('should delete', async () => {
    const errorStore = useErrorStore();
    const anId = "anId";
    const aKey = "aKey";
    const wrapper = shallowMount(KubosManage, {
      props: {
        id: anId,
        kkey: aKey,
      },
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.isDeleteDisabled).toEqual(true);
    wrapper.vm.isDeleteConfirmed = true;
    expect(wrapper.vm.isDeleteDisabled).toEqual(false);
    mockedAxios.delete.mockImplementationOnce(() => Promise.resolve());
    wrapper.vm.deleteKubos();
    expect(wrapper.vm.isDeleteDisabled).toEqual(true);
    await wrapper.vm.$nextTick();
    expect(mockedAxios.delete).toHaveBeenCalledTimes(1);
    expect(mockedAxios.delete).toHaveBeenCalledWith(kubosUrl + anId, { params: { key: aKey } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isDeleteDisabled).toEqual(false);
    expect(mockRouterPush).toHaveBeenCalledWith({ name: "KubosCreate" });
    expect(errorStore.errorMessage).toBeNull();
  });

  it('should delete detecting errors', async () => {
    const errorStore = useErrorStore();
    const anId = "anId";
    const aKey = "aKey";
    const wrapper = shallowMount(KubosManage, {
      props: {
        id: anId,
        kkey: aKey,
      },
      global: {
        stubs: ['font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    const anError = "An error";
    expect(wrapper.vm.isDeleteDisabled).toEqual(true);
    wrapper.vm.isDeleteConfirmed = true;
    expect(wrapper.vm.isDeleteDisabled).toEqual(false);
    mockedAxios.delete.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    consoleErrorSpy.mockImplementationOnce(() => { });
    wrapper.vm.deleteKubos();
    expect(wrapper.vm.isDeleteDisabled).toEqual(true);
    await wrapper.vm.$nextTick();
    expect(mockedAxios.delete).toHaveBeenCalledTimes(1);
    expect(mockedAxios.delete).toHaveBeenCalledWith(kubosUrl + anId, { params: { key: aKey } });
    await wrapper.vm.$nextTick();
    expect(errorStore.errorMessage).toEqual(`Error: ${anError}`);
    errorStore.clearErrorMessage();
    expect(consoleErrorSpy).toHaveBeenCalled();
    expect(wrapper.vm.isDeleteDisabled).toEqual(false);
    expect(mockRouterPush).not.toHaveBeenCalled();
  });

});
