import { Card } from '../../../src/services/kubos.service';
import { shallowMount } from '@vue/test-utils';
import CardRender from '../../../src/components/CardRender.vue';

describe('Card.vue', () => {
  it('should create', async () => {
    const aCard: Card = { text: "Some text" };
    const wrapper = shallowMount(CardRender, {
      props: {
        card: aCard,
      }
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.shown).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("initial");
    wrapper.vm.toggle();
    expect(wrapper.vm.shown).toEqual(true);
  });

});
