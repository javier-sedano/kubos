import router from '../../../src/router/index';

describe('router index', () => {
  it('should create', () => {
    expect(router).toBeDefined();
  });

  it('should load lazy routes', () => {
    router.options.routes.forEach(element => {
      if (typeof element.component === "function") {
        (element.component as Function)();
      }
    });
    expect(router).toBeDefined();
  });
});
