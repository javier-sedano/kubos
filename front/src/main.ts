import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { FontAwesomeIcon, FontAwesomeLayers } from "@fortawesome/vue-fontawesome";
import { createPinia } from 'pinia';

createApp(App)
    .component("font-awesome-icon", FontAwesomeIcon)
    .component('font-awesome-layers', FontAwesomeLayers)
    .use(router)
    .use(createPinia())
    .mount('#app');
