import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/privacy',
    name: 'Privacy',
    component: () => import(/* webpackChunkName: "privacy" */ '../views/Privacy.vue'),
  },
  {
    path: '/:id',
    name: 'KubosView',
    component: () => import(/* webpackChunkName: "kubosView" */ '../views/KubosView.vue'),
  },
  {
    path: '/',
    name: 'KubosCreate',
    component: () => import(/* webpackChunkName: "kubosCreate" */ '../views/KubosCreate.vue'),
  },
  {
    path: '/:catchAll(.*)',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "notFound" */ '../views/NotFound.vue'),
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
