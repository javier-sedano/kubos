import axios, { AxiosError } from "axios";
import constants from "./service.constants"

const url = constants.restBaseUrl + "/kubos/";

export interface KubosCreateResponse {
    id: string,
    key: string,
}

export interface Kubos {
    id: string,
    title: string,
    cards?: Card[],
}

export interface Card {
    text: string,
}

export interface AddResponse {
    result: string;
}

export class KubosService {
    async create(title: string): Promise<KubosCreateResponse> {
        return (await axios.post(url, { title: title })).data;
    }
    async get(id: string, key: string | null): Promise<Kubos | null> {
        try {
            return (await axios.get(url + id, { params: { key: key } })).data;
        } catch (error) {
            if (error instanceof AxiosError
                && (error as AxiosError).response?.status === 404) {
                return null;
            } else {
                throw error;
            }
        }
    }
    add(id: string, cardText: string): Promise<AddResponse> {
        return axios.put(url + id, { text: cardText });
    }
    delete(id: string, key: string): Promise<void> {
        return axios.delete(url + id, { params: { key: key } });
    }
}

export const kubosService: KubosService = new KubosService();
