import axios from "axios";
import constants from "./service.constants"

const url = constants.restBaseUrl + "/version";
export class VersionService {
    async getVersion(): Promise<string> {
        return (await axios.get(url)).data;
    }
}

export const versionService: VersionService = new VersionService();
