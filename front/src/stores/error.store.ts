import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useErrorStore = defineStore('error', () => {
    const errorMessage = ref<string | null>(null);

    function setErrorMessage(message: string | null): void {
        errorMessage.value = message;
    }

    function clearErrorMessage(): void {
        setErrorMessage(null);
    }

    return { errorMessage, setErrorMessage, clearErrorMessage };
})
