const versionService = require('../../../src/services/version.service');
const fs = require('fs');

jest.mock('fs');

afterEach(() => {
    jest.clearAllMocks();
});

describe("version service unit test", function () {
    it("should return a version", function (done) {
        const A_VERSION = "1.2.3";
        fs.readFile.mockImplementationOnce((dir, callback) => {
            callback(undefined, A_VERSION);
        });
        versionService.getVersion(function (v) {
            expect(v).toEqual(A_VERSION);
            expect(fs.readFile).toHaveBeenCalledTimes(1);
            done();
        });
    });
});
