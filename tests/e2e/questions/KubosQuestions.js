import { t } from 'testcafe';
import kubosCreatePage from '../pages/KubosCreatePage';
import kubosUserPage from '../pages/KubosUserPage';
import kubosOwnerPage from '../pages/KubosOwnerPage';

class KubosQuestions {
    async createIsShown() {
        await t.expect(kubosCreatePage.channelTitle.exists).ok();
    }

    async ownerPageIsShown(title) {
        await t.expect(kubosOwnerPage.userLink.exists).ok()
            .expect(kubosOwnerPage.ownerLink.exists).ok()
        await t.expect(kubosOwnerPage.kubosTitle.innerText).eql(title);
        const userLink = await kubosOwnerPage.userLinkUrl();
        const ownerLink = await kubosOwnerPage.ownerLinkUrl();
        return { userLink, ownerLink };
    }

    async userPageIsShown(title) {
        await t.expect(kubosUserPage.cardText.exists).ok();
        await t.expect(kubosUserPage.kubosTitle.innerText).eql(title);
    }

    async cardExists(text) {
        await t.expect(kubosOwnerPage.cardWith(text).exists).ok();
    }

    async cardNotExists(text) {
        await t.expect(kubosOwnerPage.cardWith(text).exists).notOk();
    }

}

export default new KubosQuestions();
