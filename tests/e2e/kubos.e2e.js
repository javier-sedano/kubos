import kubosTasks from './tasks/KubosTasks';
import kubosQuestions from './questions/KubosQuestions';

fixture`Kubos`
    .page`http://localhost:4011/kubos/web`;

test('Main flow: create, add cards, show cards, delete', async t => {
    await kubosQuestions.createIsShown();

    const title = "A channel title";
    await kubosTasks.createChannel(title);
    const { userLink, ownerLink } = await kubosQuestions.ownerPageIsShown(title);

    await kubosTasks.goToUserPage(userLink);
    await kubosQuestions.userPageIsShown(title);

    const text1 = "Some text for a card";
    await kubosTasks.submitCard(text1);
    await kubosQuestions.userPageIsShown(title);

    const text2 = "Some other text for a card";
    await kubosTasks.submitCard(text2);
    await kubosQuestions.userPageIsShown(title);

    await kubosTasks.goToOwnerPage(ownerLink);
    await kubosQuestions.ownerPageIsShown(title);
    await kubosQuestions.cardExists(text1);
    await kubosQuestions.cardExists(text2);
    await kubosQuestions.cardNotExists("Not a card");

    await kubosTasks.deleteChannel();
    await kubosQuestions.createIsShown();
});
