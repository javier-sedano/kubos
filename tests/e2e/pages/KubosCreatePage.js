import { Selector, t } from 'testcafe';

class KubosCreatePage {
    constructor() {
        this.channelTitle = Selector("#newchannel-title");
        this.createButton = Selector("#newchannel-create");
    }
}

export default new KubosCreatePage();
