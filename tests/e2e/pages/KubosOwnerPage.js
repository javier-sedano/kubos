import { Selector, t } from 'testcafe';
import { KubosPage } from './KubosPage';

class KubosOwnerPage extends KubosPage {
    constructor() {
        super();
        this.userLink = Selector("#user-link");
        this.ownerLink = Selector("#owner-link");
        this.deleteConfirm = Selector("#kubos-delete-confirm");
        this.delete = Selector("#kubos-delete");
        this.card = Selector("div.app-card-text");
    }

    async userLinkUrl() {
        return await this.userLink.getAttribute("href");
    }

    async ownerLinkUrl() {
        return await this.ownerLink.getAttribute("href");
    }

    cardWith(text) {
        return this.card.withExactText(text);
    }
}

export default new KubosOwnerPage();
