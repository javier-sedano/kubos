import { Selector } from 'testcafe';

export class KubosPage {
    constructor() {
        this.kubosTitle = Selector("#kubos-title");
    }
}

export default new KubosPage();
