import { Selector, t } from 'testcafe';
import { KubosPage } from './KubosPage';

class KubosUserPage extends KubosPage {
    constructor() {
        super();
        this.cardText = Selector("#newcard-text");
        this.cardAdd = Selector("#newcard-add");
    }
}

export default new KubosUserPage();
