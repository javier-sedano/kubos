import { t } from 'testcafe';
import kubosCreatePage from '../pages/KubosCreatePage';
import kubosUserPage from '../pages/KubosUserPage';
import kubosOwnerPage from '../pages/KubosOwnerPage';

class KubosTasks {

    async createChannel(title) {
        await t.
            typeText(kubosCreatePage.channelTitle, title)
            .click(kubosCreatePage.createButton);
    }

    async goToUserPage(userLink) {
        await t.navigateTo(userLink);
    }

    async goToOwnerPage(ownerLink) {
        await t.navigateTo(ownerLink);
    }

    async submitCard(text) {
        await t.
            typeText(kubosUserPage.cardText, text)
            .click(kubosUserPage.cardAdd);
    }

    async deleteChannel() {
        await t
            .click(kubosOwnerPage.deleteConfirm)
            .click(kubosOwnerPage.delete);
    }
}

export default new KubosTasks();
