const { v4: uuidv4 } = require('uuid');
const winstonConfigurer = require('../../../src/config/winston.configurer');
const kubosService = require('../../../src/services/kubos.service');
const mongo = require('../../../src/db/mongo');
const constants = require('../../../src/constants');

const KUBOS_COLLECTION = "kubos";

describe("kubos service integration test", () => {
    beforeAll(async (done) => {
        winstonConfigurer.configure();
        await mongo.connect();
        done();
    });

    afterAll(async (done) => {
        await mongo.disconnect();
        done();
    });

    it("should create a kubos", async (done) => {
        const aKubos = { title: "A title " + uuidv4() };
        try {
            const now = new Date().getTime();
            const res = await kubosService.create(aKubos);
            const foundKubos = await mongo.db().collection(KUBOS_COLLECTION).findOne({ id: res.id });
            expect(foundKubos.id).toEqual(res.id);
            expect(foundKubos.title).toEqual(aKubos.title);
            expect(foundKubos.key).toEqual(res.key);
            expect(foundKubos.cards).toEqual([]);
            expect(foundKubos.lastModification >= now).toEqual(true);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ title: aKubos.title });
        }
        done();
    });

    it("Should obtain a basic kubos without key", async (done) => {
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [{ text: "Some text" }] };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            const retrievedKubos = await kubosService.retrieve(aKubos.id);
            expect(retrievedKubos.id).toEqual(aKubos.id);
            expect(retrievedKubos.title).toEqual(aKubos.title);
            expect(retrievedKubos.key).not.toBeDefined();
            expect(retrievedKubos.cards).not.toBeDefined();
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

    it("Should obtain a complete kubos with key", async (done) => {
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [{ text: "Some text" }] };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            const retrievedKubos = await kubosService.retrieve(aKubos.id, aKubos.key);
            expect(retrievedKubos.id).toEqual(aKubos.id);
            expect(retrievedKubos.title).toEqual(aKubos.title);
            expect(retrievedKubos.key).not.toBeDefined();
            expect(retrievedKubos.cards).toEqual(aKubos.cards);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

    it("Should obtain a basic kubos with wrong key", async (done) => {
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [{ text: "Some text" }] };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            const retrievedKubos = await kubosService.retrieve(aKubos.id, "wrong" + aKubos.key);
            expect(retrievedKubos.id).toEqual(aKubos.id);
            expect(retrievedKubos.title).toEqual(aKubos.title);
            expect(retrievedKubos.key).not.toBeDefined();
            expect(retrievedKubos.cards).not.toBeDefined();
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

    it("Should not found nonexistent kubos", async (done) => {
        const retrievedKubos = await kubosService.retrieve("nonExistent");
        expect(retrievedKubos).toBeNull();
        done();
    });

    it("Should not allow duplicated id", async (done) => {
        await kubosService.initDb();
        const oneId = "oneId";
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne({ id: oneId });
            try {
                await mongo.db().collection(KUBOS_COLLECTION).insertOne({ id: oneId });
            } catch (e) {
                const UNIQUE_ERROR_CODE = 11000;
                expect(e.code).toEqual(UNIQUE_ERROR_CODE);
            }
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteMany({ id: oneId });
        }
        done();
    });

    it("Should delete with key", async (done) => {
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [{ text: "Some text" }] };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            await kubosService.delete(aKubos.id, aKubos.key);
            const foundKubos = await mongo.db().collection(KUBOS_COLLECTION).findOne({ id: aKubos.id });
            expect(foundKubos).toBeNull();
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

    it("Should not delete with wrong key", async (done) => {
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [{ text: "Some text" }] };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            await kubosService.delete(aKubos.id, "wrong" + aKubos.key);
            const foundKubos = await mongo.db().collection(KUBOS_COLLECTION).findOne({ id: aKubos.id });
            expect(foundKubos.id).toEqual(aKubos.id);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

    it("Should add card to a kubos", async (done) => {
        const now = new Date().getTime();
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [], lastModification: 0 };
        const aCard = { text: "Some text" };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            await kubosService.addCard(aKubos.id, aCard);
            const foundKubos = await mongo.db().collection(KUBOS_COLLECTION).findOne({ id: aKubos.id });
            expect(foundKubos.cards).toEqual([aCard]);
            expect(foundKubos.lastModification >= now).toEqual(true);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

    it("Should delete old kubos", async (done) => {
        const now = new Date().getTime();
        const anOldKubos = { id: "oldId1", title: "Old title", key: "0ldk3y", cards: [], lastModification: (now - constants.oldThreshold - 1) };
        const anotherOldKubos = { id: "oldId2", title: "Old title", key: "0ldk3y", cards: [], lastModification: (now - constants.oldThreshold - 2) };
        const aNewKubos = { id: "newId", title: "New title", key: "0ldk3y", cards: [], lastModification: (now - constants.oldThreshold + (1000 * 60 * 60)) };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(anOldKubos);
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(anotherOldKubos);
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aNewKubos);
            await kubosService.deleteOldKuboses();
            const foundKuboses = await mongo.db().collection(KUBOS_COLLECTION).find().toArray();
            expect(foundKuboses.length).toEqual(1);
            expect(foundKuboses[0].id).toEqual(aNewKubos.id);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: anOldKubos.id });
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: anotherOldKubos.id });
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aNewKubos.id });
        }
        done();
    });

    it("Should count kubos", async (done) => {
        const aKubos = { id: "1d", title: "A title", key: "k3y", cards: [{ text: "Some text" }] };
        try {
            await mongo.db().collection(KUBOS_COLLECTION).insertOne(aKubos);
            const kubosAmmount = await kubosService.count();
            expect(kubosAmmount).toEqual(1);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: aKubos.id });
        }
        done();
    });

});
