const versionService = require('../../../src/services/version.service');

describe("version service integration test", function() {
    it("should return an 'undefined' version when not available", function(done) {
        var version = versionService.getVersion(function(v) {
            expect(v).toEqual("undefined");
            done();
        });
    });
});
