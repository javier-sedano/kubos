module.exports = {
    rootUrl: "/",
    baseUrl: "/kubos",
    baseRest: "/kubos/rest",
    baseWeb: "/kubos/web",
};
