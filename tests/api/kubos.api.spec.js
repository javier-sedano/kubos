const supertest = require('supertest');
const { v4: uuidv4 } = require('uuid');
const app = require('../../app');
const winstonConfigurer = require('../../src/config/winston.configurer');
const apiConstants = require('./api.constants');
const mongo = require('../../src/db/mongo');

const url = apiConstants.baseRest + '/kubos';
const KUBOS_COLLECTION = "kubos";

describe("/kubos API test", function () {

    beforeAll(async (done) => {
        winstonConfigurer.configure();
        await mongo.connect();
        done();
    });

    afterAll(async (done) => {
        await mongo.disconnect();
        done();
    });

    it("should do the crud cycle", async function (done) {
        async function create() {
            const postResponse = (
                await supertest(app)
                    .post(url + "/")
                    .send({ title: title })
                    .expect(200)
            ).body;
            expect(postResponse.id).toBeDefined();
            expect(postResponse.key).toBeDefined();
            return postResponse;
        }

        async function ownerGet(id, key, expectedTitle, expectedCards) {
            const ownerGetResponse = (
                await supertest(app)
                    .get(url + "/" + id)
                    .query({ key: key })
                    .expect(200)
                    .expect('Cache-Control', 'no-store, no-cache, must-revalidate')
            ).body;
            expect(ownerGetResponse.id).toEqual(id);
            expect(ownerGetResponse.title).toEqual(expectedTitle);
            expect(ownerGetResponse.cards).toEqual(expectedCards);
        }

        async function userGet(id, expectedTitle) {
            const userGetResponse = (
                await supertest(app)
                    .get(url + "/" + id)
                    .expect(200)
                    .expect('Cache-Control', 'no-store, no-cache, must-revalidate')
            ).body;
            expect(userGetResponse.id).toEqual(id);
            expect(userGetResponse.title).toEqual(expectedTitle);
            expect(userGetResponse.cards).not.toBeDefined();
        }

        async function userGetWrongKey(id, key, expectedTitle) {
            const userGetResponse = (
                await supertest(app)
                    .get(url + "/" + id)
                    .query({ key: "wrongKey" })
                    .expect(200)
            ).body;
            expect(userGetResponse.id).toEqual(id);
            expect(userGetResponse.title).toEqual(expectedTitle);
            expect(userGetResponse.cards).not.toBeDefined();
        }

        async function putWithoutCard(id) {
            await supertest(app)
                .put(url + "/" + id)
                .expect(400);
        }

        async function putWithCard(id, cardText) {
            await supertest(app)
                .put(url + "/" + id)
                .send({ text: cardText })
                .expect(200);

        }

        async function deleteWithoutKey(id) {
            await supertest(app)
                .delete(url + "/" + id)
                .expect(200);
        }

        async function deleteWithKey(id, key) {
            await supertest(app)
                .delete(url + "/" + id)
                .query({ key: key })
                .expect(200);
        }

        async function getDeleted(id, key) {
            await supertest(app)
                .get(url + "/" + id)
                .query({ key: key })
                .expect(404);
        }

        const title = "Title " + uuidv4();
        const cardText = "Some text";
        try {
            const postResponse = await create();
            await ownerGet(postResponse.id, postResponse.key, title, []);
            await userGet(postResponse.id, title);
            await userGetWrongKey(postResponse.id, "wrongKey", title);
            await putWithoutCard(postResponse.id);
            await putWithCard(postResponse.id, cardText);
            await ownerGet(postResponse.id, postResponse.key, title, [{ text: cardText }]);
            await deleteWithoutKey(postResponse.id);
            await ownerGet(postResponse.id, postResponse.key, title, [{ text: cardText }]);
            await deleteWithKey(postResponse.id, postResponse.key);
            await getDeleted(postResponse.id, postResponse.key);
        } finally {
            await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ title: title });
        }
        done();
    });

    it("should reject post without title", async function (done) {
        await supertest(app).post(url + "/").expect(400);
        done();
    });

    it("should reject get without id", async function (done) {
        await supertest(app).get(url + "/").expect(404);
        done();
    });

    it("should reject get with wrong id", async function (done) {
        await supertest(app).get(url + "/wrongId").expect(404);
        done();
    });

    it("should reject put without id", async function (done) {
        await supertest(app).put(url + "/").send({ text: "Some text" }).expect(404);
        done();
    });

    it("should ignore put with wrong id", async function (done) {
        await supertest(app).put(url + "/wrongId").send({ text: "Some text" }).expect(200);
        done();
    });

    it("should reject delete without id", async function (done) {
        await supertest(app).delete(url + "/").expect(404);
        done();
    });

    it("should ignore delete with wrong id", async function (done) {
        await supertest(app).delete(url + "/wrongId").expect(200);
        done();
    });
});
