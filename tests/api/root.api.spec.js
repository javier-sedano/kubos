const app = require('../../app');
const supertest = require('supertest');
const apiConstants = require('./api.constants');

describe("root API test", function () {
    it("should redirect root to base", function (done) {
        supertest(app).get(apiConstants.rootUrl)
            .expect(302)
            .expect('Location', apiConstants.baseUrl)
            .end(done);
    });

    it("should redirect base to web", function (done) {
        supertest(app).get(apiConstants.baseUrl)
            .expect(302)
            .expect('Location', apiConstants.baseWeb)
            .end(done);
    });

});
