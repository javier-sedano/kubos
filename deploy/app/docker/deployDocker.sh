#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi

REGISTRY=registry.gitlab.com/javier-sedano/kubos/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=kubos" --filter "status=running" --quiet`
docker container stop kubos
docker container rm kubos
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 32m -p 5880:4011 --env-file ./kubos.env --restart unless-stopped --name kubos $IMAGE
if [ "$containerId" ]
then
  docker container start kubos
fi
