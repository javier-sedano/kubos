module.exports = {
    rootUrl: "/",
    baseUrl: "/kubos",
    baseRest: "/kubos/rest",
    baseWeb: "/kubos/web",
    oldThreshold: 1000*60*60*24*30,
};