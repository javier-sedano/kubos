const MongoClient = require('mongodb').MongoClient;
const URL = process.env.KUBOS_MONGO_URL || "mongodb://root:toor@mongo:27017/";
const DATABASE = process.env.KUBOS_MONGO_DATABASE || "kubos";
const logger = require('winston');

let client;
let db;

module.exports = {
    connect: async function () {
        logger.debug("Connecting to mongo " + URL.replace(/:\/\/.*@/, "://****@") + " DATABASE " + DATABASE + "...");
        client = await MongoClient.connect(URL);
        logger.debug("Connected");
        db = client.db(DATABASE);
    },
    disconnect: async function () {
        logger.debug("Disconnecting from mongo...");
        await client.close();
    },
    db: function () {
        return db;
    },
};