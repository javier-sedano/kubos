const express = require('express');
const router = express.Router();
const versionService = require('../services/version.service');

router.get('/', (_req, res, _next) => {
  versionService.getVersion((version) => {
    res.send(version);
  });
});

module.exports = router;
