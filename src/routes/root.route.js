const express = require('express');
const constants = require('../constants');
const router = express.Router();

router.get(constants.rootUrl, (_req, res, _next) => {
  res.redirect(constants.baseUrl);
});

router.get(constants.baseUrl, (_req, res, _next) => {
  res.redirect(constants.baseWeb);
});

module.exports = router;
