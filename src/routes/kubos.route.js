const express = require('express');
const router = express.Router();
const kubosService = require('../services/kubos.service');
const logger = require('winston');

router.post('/', (req, res, _next) => {
  const title = req.body.title;
  if (title === undefined) {
    res.sendStatus(400);
    return;
  }
  kubosService.create({ title: title }).then((createdKubos) => {
    res.send(createdKubos);
  });
});

router.get('/:id', (req, res, _next) => {
  kubosService.retrieve(req.params.id, req.query.key).then((retrievedKubos) => {
    if (retrievedKubos === null) {
      res.sendStatus(404);
      return;
    }
    res.set('Cache-Control', 'no-store, no-cache, must-revalidate');
    res.send(retrievedKubos);
  });
});

router.put('/:id', (req, res, _next) => {
  const text = req.body.text;
  const id = req.params.id;
  if (text === undefined) {
    res.sendStatus(400);
    return;
  }
  kubosService.retrieve(id).then((retrievedKubos) => {
    if (retrievedKubos === null) {
      res.sendStatus(200);
      return;
    }
    kubosService.addCard(id, { text: text }).then(() => {
      res.sendStatus(200);
    });
  });
});

router.delete('/:id', (req, res, _next) => {
  kubosService.delete(req.params.id, req.query.key).then((retrievedKubos) => {
    res.send(retrievedKubos);
  });
});

module.exports = router;
