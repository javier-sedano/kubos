const logger = require('winston');
const { v4: uuidv4 } = require('uuid');
const mongo = require('../db/mongo');
const constants = require('../constants');

const KUBOS_COLLECTION = "kubos";

function KubosService() {
    this.initDb = async () => {
        const result = await mongo.db().collection(KUBOS_COLLECTION).createIndex({ id: 1 }, { name: "kubos_id", unique: true });
        logger.debug("kubos unique created with result: " + result);
    };
    this.create = async (kubos) => {
        const id = uuidv4();
        const key = uuidv4();
        const newKubos = {
            id: id,
            key: key,
            title: kubos.title,
            cards: [],
            lastModification: new Date().getTime(),
        }
        await mongo.db().collection(KUBOS_COLLECTION).insertOne(newKubos);
        return {
            id: id,
            key: key
        };
    };
    this.retrieve = async (id, key) => {
        const foundKubos = await mongo.db().collection(KUBOS_COLLECTION).findOne({ id: id });
        if (foundKubos === null) {
            return null;
        }
        const resultKubos = {
            id: foundKubos.id,
            title: foundKubos.title
        }
        if (key === foundKubos.key) {
            resultKubos.cards = foundKubos.cards;
        }
        return resultKubos;
    };
    this.addCard = async (id, card) => {
        await mongo.db().collection(KUBOS_COLLECTION).updateOne(
            { id: id },
            { $set: { lastModification: new Date().getTime() } }
        );
        await mongo.db().collection(KUBOS_COLLECTION).updateOne(
            { id: id },
            { $push: { cards: card } }
        );
    };
    this.delete = async (id, key) => {
        await mongo.db().collection(KUBOS_COLLECTION).deleteOne({ id: id, key: key });
    };
    this.deleteOldKuboses = async () => {
        logger.debug("Deleting old kuboses");
        const oldThreshold = new Date().getTime() - constants.oldThreshold;
        await mongo.db().collection(KUBOS_COLLECTION).deleteMany({ lastModification: { $lte: oldThreshold } });
    };
    this.count = async () => {
        logger.debug("Counting kuboses");
        return mongo.db().collection(KUBOS_COLLECTION).countDocuments();
    }
}

module.exports = new KubosService();
