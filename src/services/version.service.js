const fs = require('fs');

function VersionService() {
    this.getVersion = (callback) => {
        fs.readFile(__dirname + '/../../gen/version.txt', (_err, data) => {
            callback('' + data);
        })
    };
}

module.exports = new VersionService();
