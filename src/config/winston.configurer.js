
module.exports = {
    configure() {
        const winston = require('winston');
        const LOGLEVEL = process.env.LOGLEVEL || "error";
        const httpContext = require('express-http-context');
        const ReqIdAdder = winston.format(
            (info, _opts) => {
                info.reqId = httpContext.get('reqId');
                return info;
            }
        );
        winston.configure({
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.splat(),
                ReqIdAdder(),
                winston.format.simple()
            ),
            transports: [
                new winston.transports.Console({ level: LOGLEVEL }),
            ]
        });
    }
};