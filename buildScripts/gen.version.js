const fs = require('fs');

const version = process.argv[2] || "dev";

fs.mkdirSync(__dirname + '/../gen/', { recursive: true });
fs.writeFileSync(__dirname + '/../gen/version.txt', "" + version);
