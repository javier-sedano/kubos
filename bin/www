#!/usr/bin/env node

const app = require('../app');
const http = require('http');
const yargs = require('yargs');
const mongo = require('../src/db/mongo');
const kubosService = require('../src/services/kubos.service');

const argv = parseCommandLine();

var port = normalizePort(argv.port);
app.set('port', port);

var server = http.createServer(app);

mongo.connect().then(() => {
  initDb().then(() => {
    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
  });
});

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  require('winston').info('Listening on %s', bind);
}

async function initDb() {
  await kubosService.initDb();
}

function parseCommandLine() {
  return yargs
    .env()
    .option('host', {
      description: 'IP to listen to',
      type: 'string',
      default: '0.0.0.0'
    })
    .option('port', {
      description: 'Port to listen to',
      type: 'number',
      default: 4011,
    })
    .help()
    .alias('help', 'h')
    .argv;
}

module.exports = server
