Kudos-like simple service

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/kubos/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/kubos/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.kubos&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.kubos)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.kubos)
</details>

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
  * Chrome with Vue.js devtools plugin
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* NPM SCRIPTS will show the available tasks:
  * Run "dependenciesInit" to download dependencies
  * Run "start:dev" to start serving (while watching for changes in both frontend and backend)
    * Most common development tasks are configured as npm tasks
  * Browse http://localhost:4011/kubos/
  * mongo-express is available at http://localhost:9012/ . mongo-shell is installed in the container (mongo --host mongo --username root --password toor).
* When running sonnar for the first time, the password of the user `admin` will be set to `admin1` (which is needed by the scripts).

TODO:
* Add buttons for predefined input texts (Thanks, Congrats, Awesome,...)

Useful links:
* https://getinsights.io/docs
